*** Settings ***
Suite Setup       Connect To Database  pymysql  ${db_name}  ${user}  ${password}  ${db_host}  ${db_port}
Suite Teardown    Disconnect From Database
Resource  ../Resource.robot

*** Variables ***
${db_name}  testdb
${db_host}  localhost
${db_port}  3306
${user}  root
${password}  Kablam0

${table}  userData

${query}  SELECT * FROM userData LIMIT 10;
${firstname}  Isabella
${Row}  4
${Column}  4

${lastname}  Verge
${lRow}  4
${lColumn}  6

*** Test Cases ***
Check Table
    table must exist  ${table}

Find First Name
    Table Query  ${query}  ${firstname}

Find First Name by Row
    Table Query With Row  ${query}  ${firstname}  ${Row}

Find First Name by Column
    Table Query With Column  ${query}  ${firstname}  ${Column}

Find First Name by Row and Column
    Table Query With Row and Column  ${query}  ${firstname}  ${Row}  ${Column}

Find Last Name
    Table Query  ${query}  ${lastname}

Find Last Name by Row
    Table Query With Row  ${query}  ${lastname}  ${lRow}

Find Last Name by Column
    Table Query With Column  ${query}  ${lastname}  ${lColumn}

Find Last Name by Row and Column
    Table Query With Row and Column  ${query}  ${lastname}  ${lRow}  ${lColumn}
