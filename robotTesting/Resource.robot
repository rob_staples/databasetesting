*** Settings ***
Library	 Collections
Library	 DatabaseLibrary
Documentation  A Basic Resouce for reading data from a table and check values against test data.

*** Keywords ***
Table Query
    [Documentation]  Find a specific value from a table with no knowledge of what row or column the value is in
    [Arguments]  ${query}  ${result}
    ${resp}=  Query  ${query}
    ${rowNum}=  convert to integer  0
    ${colNum}=  convert to integer  0
    ${resFound}=  convert to string  'false'
    :FOR  ${Rows}  IN  @{resp}
    \  ${colNum}=  _internal Call Column  ${Rows}  ${result}  ${rowNum}
    \  ${resFound}=  run keyword if  '${Rows[${colNum}]}' == '${result}'  evaluate  'true'
    \  Exit For Loop If  '${Rows[${colNum}]}' == '${result}'
    \  ${rowNum}=  Evaluate    ${rowNum} + 1
    should be true  '${resFound}' == 'true'
    Should Contain  ${resp[${rowNum}]}  ${result}

Table Query With Row
    [Documentation]  Find a specific value from a table when you know the row but don't know the column
    [Arguments]  ${query}  ${result}  ${databaseRow}
    run keyword if  ${databaseRow} < 0  fail  'Row number is a negative number'
    ${resp}=  Query  ${query}
    run keyword if  ${databaseRow} > len(${resp})  fail  'Row number is higher than number of rows in selection'
    run keyword if  $databaseRow is None  ${databaseRow}= 0
    Should Contain  ${resp[${databaseRow}]}  ${result}

Table Query With Column
    [Documentation]  Find a specific value from a table when you know the column but don't know the row
    [Arguments]  ${query}  ${result}  ${databaseColumn}
    run keyword if  ${databasecolumn} <0  fail  'Column number is a negative number'
    ${resp}=  Query  ${query}
    run keyword if  ${databaseColumn} > len(${resp[0]})  fail  'Column number is higher than number of rows in selection'
    ${rowNum}=  convert to integer  0
    ${colNum}=  convert to integer  0
    ${resFound}=  convert to string  'false'
    :FOR  ${Rows}  IN  @{resp}
    \   ${resFound}=  run keyword if  '${Rows[${databaseColumn}]}' == '${result}'  evaluate  'true'
    \   Exit For Loop If  '${Rows[${databaseColumn}]}' == '${result}'
    \   ${rowNum}=  Evaluate    ${rowNum} + 1
    should be true  '${resFound}' == 'true'
    Should Contain  ${resp[${rowNum}]}  ${result}

Table Query With Row and Column
    [Documentation]  Find a specific value from a table when you know the row and the column
    [Arguments]  ${query}  ${result}  ${databaseRow}  ${databaseColumn}

    #Check for negative number
    run keyword if  ${databaseRow} < 0  fail  'Row number is a negative number'
    run keyword if  ${databasecolumn} <0  fail  'Column number is a negative number'
    ${resp}=  Query  ${query}

    #Check for out of range
    run keyword if  ${databaseRow} > len(${resp})  fail  'Row number is higher than number of rows in selection'
    run keyword if  ${databaseColumn} > len(${resp[${databaseRow}]})  fail  'Column number is higher than number of rows in selection'
    run keyword if  $databaseRow is None  ${databaseRow}= 0
    Should Contain  ${resp[${databaseRow}][${databaseColumn}]}  ${result}

_internal Call Column
    [Arguments]  ${val}  ${result}  ${rowNum}
    ${colNum}=  convert to integer  0
    ${resFound}=  convert to string  'false'
    :FOR  ${Fields}  IN  @{val}
    \  ${resFound}=  run keyword if  '${Fields}' == '${result}'  Evaluate  'true'
    \  run keyword if  '${Fields}' == '${result}'  exit for loop
    \  ${colNum}=  Evaluate    ${colNum} + 1
    Return From Keyword If  '${resFound}' == 'true'  ${colNum}
    [Return]  0